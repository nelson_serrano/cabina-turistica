<?php
header('Content-Type: text/html; charset=utf-8' );
class Lugar {
    function publicar($data,$user){
        $idBorrador = $data['idBorrador'];

        $ultimoPrev = $this->obtenerUltimo();

        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $latLon = explode(',',$data['ubicacion']);
        $latitudLugar = $latLon[0];
        $longitudLugar = $latLon[1];


        $update = "INSERT INTO LUGAR_TURISTICO(idMunicipio, nombreLugar, telefonos, direccionLugar,
                    imagenCabecera, descriptionImagenCabecera, descripcionLugar, latitudLugarTuristico, longitudLugarTuristico)
                    VALUES ('".$data['municipio']."','".$data['titulo']."','".$data['telefono']."','".$data['direccion']
                    ."','".$data['imagenCabecera']."','".$data['descripcionImagen']."','".mysql_escape_string($data['htmldata'])."','".$latitudLugar."','".$longitudLugar."')";


        echo $update.'   /   ';

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();
        $ultimoDesp = $this->obtenerUltimo();
        if($ultimoPrev!=$ultimoDesp){
            $this->asociarCategorias($data['categorias'],$ultimoDesp);
        }

        //muevo la imagen a la carpeta donde deberia de estar
        if(strlen($data['imagenCabecera'])>0){
            rename("../View/imagenes/subidasDeUsuarios/temp/".$data['imagenCabecera'],"../View/imagenes/subidasDeUsuarios/".$data['imagenCabecera']);
        }

    }

    function asociarCategorias($categorias,$ultimoDesp){
        include 'CategoriaDao.php';
        $categoriasSeparadas = explode(',',$categorias);
        $cat = new Categoria;
        foreach($categoriasSeparadas as $categoria){
            $cat->insertarCategorias_x_lugar($categoria,$ultimoDesp);
        }
    }

    function obtenerUltimo(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT max(idLugarTuristico) max FROM LUGAR_TURISTICO";
        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results= $row['max'];
            }
        }
        $db->close();

        if (isset($results))
            return $results;
        else
            return 0;
    }

    function getLugaresDesc(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT l.idLugarTuristico, l.idMunicipio, l.nombreLugar, l.telefonos, l.direccionLugar, l.imagenCabecera, l.descriptionImagenCabecera, l.descripcionLugar,
                    l.latitudLugarTuristico, l.longitudLugarTuristico,c.imgTipoLugar, c.nombreCategoria
                    FROM  CATEGORIAS c
                    INNER JOIN CATEGORIA_X_LUGAR cxl ON (cxl.codigoCategoria=c.codigoCategoria)
                    INNER JOIN LUGAR_TURISTICO l ON (cxl.idLugarTuristico = l.idLugarTuristico)
                    ORDER BY idLugarTuristico DESC LIMIT 0,30";

        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[]= $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function getLugarDescById($idLugarTutistico){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $db->query("SET NAMES 'utf8'");
        $consulta = "SELECT idLugarTuristico, idMunicipio, nombreLugar, telefonos, direccionLugar, imagenCabecera, descriptionImagenCabecera, descripcionLugar,
                    latitudLugarTuristico, longitudLugarTuristico FROM LUGAR_TURISTICO WHERE idLugarTuristico = ".$idLugarTutistico;

        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[]= $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function getCategoriasLugaresInteres(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);
        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $lugares = 'SELECT codigoCategoria, nombreCategoria, editable, imgTipoLugar, descrTipoLugar FROM CATEGORIAS';

        $result = $db->query($lugares);
        $resultado = array();

        $i=0;
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $resultado[$i] = $row;
                $i++;
            }
        }

        $db->close();

        if (isset($resultado))
            return $resultado;

    }

    function listaLugaresCategoria($codCategoria){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT DISTINCT l.idLugarTuristico, l.idMunicipio, l.nombreLugar, l.telefonos, l.direccionLugar, l.imagenCabecera, l.descriptionImagenCabecera, l.descripcionLugar, l.latitudLugarTuristico, l.longitudLugarTuristico,c.imgTipoLugar, c.nombreCategoria
                    FROM  CATEGORIAS c
                    INNER JOIN CATEGORIA_X_LUGAR cxl ON (cxl.codigoCategoria=c.codigoCategoria)
                    LEFT JOIN LUGAR_TURISTICO l ON (cxl.idLugarTuristico = l.idLugarTuristico)
                    WHERE cxl.codigoCategoria = '$codCategoria'";

        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[]= $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }
}
?>