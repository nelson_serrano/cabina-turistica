var urlActual = '/';

function verificarPaginaActiva() {
    urlActual = (localStorage.getItem('urlActual')!== null?localStorage.getItem('urlActual'):'');
    if(urlActual.length>0 && urlActual.indexOf('admin.php')<0 && urlActual.indexOf('editorTexto.php')<0){
        cambiarDePagina(urlActual);
    }else{
        if(location.href.indexOf('admin')>0 && urlActual.indexOf('editorTexto.php')>=0){
            cambiarDePagina(urlActual);
        }else{
            localStorage.setItem('urlActual','');
        }
    }
    $('.cargandoPagina').fadeOut();
}
function cambiarDePagina(_url){
    jQuery.ajax({
        url: _url,
        type: "POST",
            beforeSend: function(){$('#salida').html('');},
            success:function(data){
                urlActual = _url
                localStorage.setItem('urlActual',_url);
                $('#salida').html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(jqXHR);
                console.error(textStatus);
                console.error(errorThrown);
                $('.cargando').children('#mensaje').html("Ocurrio un error: "+errorThrown);
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                    $('.cargando').dialog( 'close' ); }}}
                });
                $('.cargando').dialog( 'open' );
            },
    });
}
function ajaxPost(urlDest, dataDest, type,page){
    $.ajax({
        method: "post",
        cache:false,
        timeout:8000,
        url: urlDest,
        data: dataDest,
        dataType: type,
        success:function(data){ return manipularData(data,page); },
        error: function(jqXHR, textStatus, errorThrown) {
            console.error(jqXHR);
            console.error(textStatus);
            console.error(errorThrown);
            $('.cargando').children('#mensaje').html("Ocurrio un error: "+errorThrown);
            $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                $('.cargando').dialog( 'close' ); }}}
            });
            $('.cargando').dialog( 'open' );
        },
        statusCode: {
            404: function() {
                $('.cargando').children('#mensaje').html("404: Resource not found" );
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            403: function() {
                $('.cargando').children('#mensaje').html("403: Forbiden.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            408: function(){
                $('.cargando').children('#mensaje').html("408: Request Timeout.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            410: function() {
                $('.cargando').children('#mensaje').children('#mensaje').html("410: Not avaliable.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            500: function () {
                $('.cargando').children('#mensaje').html("500: Server error.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            501: function() {
                $('.cargando').children('#mensaje').html("501: Not implement.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            503: function(){
                $('.cargando').children('#mensaje').html("503: Service is offline.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            }
        }
    });
}
function ajaxPost_Sinc(urlDest, dataDest, type,page){
    $.ajax({
        method: "post",
        cache:false,
        timeout:8000,
        url: urlDest,
        data: dataDest,
        dataType: type,
        async: false,
        success:function(data){ return manipularData(data,page); },
        error: function(jqXHR, textStatus, errorThrown) {
            console.error(jqXHR);
            console.error(textStatus);
            console.error(errorThrown);
            $('.cargando').children('#mensaje').html("Ocurrio un error: "+errorThrown);
            $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                $('.cargando').dialog( 'close' ); }}}
            });
            $('.cargando').dialog( 'open' );
        },
        statusCode: {
            404: function() {
                $('.cargando').children('#mensaje').html("404: Resource not found" );
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            403: function() {
                $('.cargando').children('#mensaje').html("403: Forbiden.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            408: function(){
                $('.cargando').children('#mensaje').html("408: Request Timeout.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            410: function() {
                $('.cargando').children('#mensaje').children('#mensaje').html("410: Not avaliable.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            500: function () {
                $('.cargando').children('#mensaje').html("500: Server error.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            501: function() {
                $('.cargando').children('#mensaje').html("501: Not implement.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            },
            503: function(){
                $('.cargando').children('#mensaje').html("503: Service is offline.");
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                                $('.cargando').dialog( 'close' ); }}}});
            }
        }
    });
}
function manipularData(data,page){
    if(page==='')   return true;
    /************************************************* Login *********************************************************/
    if(page==='login'){
        if(data!=='ok' && data!=='reset'){
            $('.cargando').children('#mensaje').html(data);
            $('.cargando').dialog({buttons: { "Ok": {  text: 'Ok', class: 'btn btn-link', click: function () {
                $('.cargando').dialog( 'close' );
                $('.cargando').children('#mensaje').html('<span id="textoMensaje" >Validating user and password</span>');
                $('#animacion').show();
            }}}});
            $('.cargando').dialog( "open" );
        }else{
            var _url = 'View/Administracion/admin.php';
            localStorage.setItem('urlActual','View/Administracion/admin.php');
            location.href = window.location.protocol+'//'+window.location.hostname+'/'+_url;
        }
        return true;
    }
    if(page==='obtenerMunicipio'){
        $('.generalidades').children('#municipio').html(data);
        return true;
    }
    if(page==='getDescripcion'){
        $('.descriptLugar').children('#todoDescrp').html(data);
        return true;
    }
    if(page==='getLugaresInteres'){
        $('.list-group').children('#list-LugInteres').html(data);
        return true;
    }
    if(page==='vistaPrevia'){
        var json = JSON.parse(data);

        var html = '<html><head><title>--- Preview --- '+json['titulo']+'</title></head><body>'
                    +CKEDITOR.instances.editor.document.getBody().getHtml()
                    +'</body></html>';
        var uri = "data:text/html," + encodeURIComponent(html);
        var newWindow = window.open(uri);

        //if(json['resultado'].length>0){}

        $('#idBorrador').val(json['idBorrador']);
        $('#idBorrador').attr('value',json['idBorrador']);

        return true;
    }
    if(page==='guardarBorrador'){
        $('#idBorrador').val(data);
        $('#idBorrador').attr('value',data); // me aseguro que no vaya a dar conflicto y que cambie hasta el atributo value
        return true;
    }
    if(page==='obtenerNoticiasAdmin'){
        $('.collapse#collapseTwo').children('.panel-body').html(data);
    }
    if(page==='obtenerNoticias'){
        $('.noticias_wrapper').html(data);
    }
    if(page==='obtenerLugaresAdmin'){
        $('.collapse#collapseOne').children('.panel-body').html(data);
    }
}
function redimencionarElementos(){
    wHeight = $(window).height();
    wWidth = $(window).width();

    if($('.fondoEditor').length>0){
        $('.fondoEditor').css('height', (wHeight-128)+'px');
    }
    if($('.gllpMap').length>0){
        $('.gllpMap').css('height', (wHeight-168)+'px');
    }
    //.gllpMap
}
function getLocation(latitud, longitud) {
    latlon = new google.maps.LatLng(latitud, longitud)
    mapholder = document.getElementById('mapDesc')
    mapholder.style.height = '350px';
    mapholder.style.width = '100%';

    var myOptions = {
        center:latlon,
        zoom:14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        zoomControl:true,
        mapTypeControl:true,
        mapTypeControlOptions: {
          style:google.maps.MapTypeControlStyle.DEFAULT
        },
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }

    var map = new google.maps.Map(document.getElementById("mapDesc"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map});
}
$('body').delegate('#lugaresDeInteres','click',function (event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/categorias.php';
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('#fiestasPatronales','click',function (event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/fiestasPatronales.php';
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('.lugarEscogido','click',function (event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/listaLugaresCategoria.php?categoria='+$(this).attr('codigocategoria');
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('.descripcionLugar', 'click',function (event) {
   event.preventDefault();
   var _url = location.protocol+'//'+location.host+'/View/Contenido/descripcionLugar.php?idLugar='+$(this).attr('idlugarturistico');
   localStorage.setItem('urlActual',_url);
   cambiarDePagina(_url);
});
$('body').delegate('#lugaresDeComida','click',function (event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/listaLugaresCategoria.php?categoria=COM';
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('.navbar-brand','click',function(event) {
    event.preventDefault();
    localStorage.setItem('urlActual','');
    location.href = location.protocol+'//'+location.hostname+'/';
});
$('body').delegate('.botonAtras','click',function () {
    console.log('entra');
    if(urlActual.indexOf('idLugar')>0 || urlActual.indexOf('categoria=')>0){
        cambiarDePagina(location.protocol+'//'+location.host+'/View/Contenido/categorias.php');
    }else{
        var url = location.href;
        if(urlActual.indexOf('categorias.php')>0){
            if(url.indexOf('Administracion/admin.php')>0){
                event.preventDefault();
                localStorage.setItem('urlActual','');
                location.reload();
            }else{
                event.preventDefault();
                localStorage.setItem('urlActual','');
                location.href=window.location.protocol+'//'+window.location.hostname+'/';
            }
        }else{
            event.preventDefault();
            localStorage.setItem('urlActual','');
            location.href=window.location.protocol+'//'+window.location.hostname+'/';
        }
    }
});
$('body').delegate('#inicio','click',function(event) {
    event.preventDefault();
    localStorage.setItem('urlActual','');
    location.href=window.location.protocol+'//'+window.location.hostname+'/';
});
$('body').delegate('#categorias','click',function(event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/categorias.php';
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('#lugares','click',function(event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/listaLugaresCategoria.php';
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('body').delegate('.noticia','click',function(event) {
    event.preventDefault();
    var _url = location.protocol+'//'+location.host+'/View/Contenido/noticias.php?idNoticia='+$(this).attr('idNoticia');
    localStorage.setItem('urlActual',_url);
    cambiarDePagina(_url);
});
$('[data-toggle="tooltip"]').tooltip();
