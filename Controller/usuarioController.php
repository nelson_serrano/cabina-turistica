<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    if(isset($_SESSION['usuario'])){
        $user = $_SESSION['usuario'];
        $_SESSION['usuario'] = $user;
    }

    include_once '../Model/usuarioDao.php';

    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if (strcmp($action,'insertar')==0){
            $json = $_POST['data'];

            $json = str_replace('\"','"',$json);
            $jsonDecode = json_decode($json, true);

            $usuario = new Usuario;
            $usuario->insertarUsuario($jsonDecode, $user);

        }elseif(strcmp($action,'actualizar')==0){
            $json = $_POST['data'];

            $json = str_replace('\"','"',$json);
            $jsonDecode = json_decode($json, true);

            $usuario = new Usuario;
            $usuario->actualizarUsusario($jsonDecode, $user);
        }elseif(strcmp($action,'eliminar')==0){
            $json = $_POST['data'];

            $json = str_replace('\"','"',$json);
            $jsonDecode = json_decode($json, true);

            $usuario = new Usuario;
            $usuario->eliminarUsuario($jsonDecode['idUsuario'],$user);
        }
    }
?>