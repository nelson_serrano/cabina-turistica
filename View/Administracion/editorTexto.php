        <div class="row">
            <div class="tituloEntrada_contenedor">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <label for="titulo">Entrada:</label>
                    <input type="text" name="idBorrador" id="idBorrador" value="0" style="display:none;"/>
                    <input type="text" class="form-control" id="titulo" placeholder="Titulo de la entrada o nombre del lugar">
                </div>
                <div class="botones_wrapper col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="botonesAccion btn btn-default cerrar">Cerrar</div>
                    <div class="botonesAccion btn btn-warning publicar">Publicar</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="fondoEditor">
                <div class="col-sm-2 col-md-1 col-lg-1"></div>
                <div name="entrada" id="entrada" class="visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <textarea name="editor" id="editor">
                  </textarea>
                </div>
                <div class="configuracionEntrada_wrapper visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="configuracionEntrada">
                      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="Generalidades">
                              <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  Generalidades
                                </a>
                              </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="Generalidades">
                              <div class="panel-body">

                                <div class="generalidades">
                                  Departamento
                                  <select class="form-control" id="departamento">
                                    <?php
                                    include_once "../../Model/DepartamentoDao.php";
                                    include_once "../../Model/MunicipioDao.php";

                                    $dep = new Departamento;
                                    $result = $dep->obtenerDepartamentos();
                                    foreach ($result as $departamento) {
                                      echo '<option value="'.$departamento['codDepartamento'].'">'
                                            .$departamento['nombreDepartamento']
                                            .'</option>';
                                    }
                                    ?>
                                  </select>
                                  </br>
                                  Municipio
                                  <select class="form-control" id="municipio">
                                  </select>
                                  </br>
                                  Imagen principal
                                  <div class="imagenCabecera" id="imagenCabecera">
                                    <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
                                  </div>
                                  <input type="text" class="form-control" id="descripcionImagen" placeholder="Descripcion imagen">
                                </div>

                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="tipoEntrada">
                              <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  Tipo de Entrada
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tipoEntrada">
                              <div class="panel-body">
                                Publicar como:
                                </br>
                                <div class="btn-group" id="tipoPublicacion" data-toggle="buttons">
                                  <label class="btn btn-default active" id="radio_noticia">
                                    <input type="radio" name="options" id="option1" autocomplete="off" value="noticia" checked> Noticia
                                  </label>
                                  <label class="btn btn-default" id="radio_lugar">
                                    <input type="radio" name="options" id="option2" autocomplete="off" value="lugar"> Lugar
                                  </label>
                                </div>
                                </br>
                                <div class="datosLugar">
                                  <input type="text" class="form-control" id="telefono" placeholder="Telefonos">
                                  <input type="text" class="form-control" id="direccion" placeholder="Direccion">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="categoriasEntrada">
                              <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  Categorias
                                </a>
                              </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="categoriasEntrada">
                              <div class="panel-body">
                                <div class="btn-group" data-toggle="buttons">

                                 <?php
                                  include_once "../../Model/CategoriaDao.php";

                                  $categoria = new Categoria;
                                  $result = $categoria->obtenerCategorias();
                                  foreach ($result as $cat) {
                                      echo '<label class="botonCategorias btn btn-default" codigoCategoria="'.$cat['codigoCategoria'].'">
                                              <input type="checkbox" autocomplete="off" checked> '.$cat['nombreCategoria'].'
                                            </label>';
                                    }
                                 ?>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="ubicacionEntrada">
                              <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                  Ubicacion en el mapa
                                </a>
                              </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ubicacionEntrada">
                              <div class="panel-body">
                                <button type="button" class="btn btn-link" id="ubicacionMapa">
                                  <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Obtener Ubicacion
                                </button>
                                <input type="text" name="ubicacion" id="ubicacion" disabled/>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mapaSelector">
          <div class="contenedor_mapa">
            <blockquote>
              <footer>Haga doble click para seleccionar la ubicacion</footer>
            </blockquote>
            <fieldset class="gllpLatlonPicker">
            <div class="gllpMap">Google Maps</div>
            <div class="informacionMapa">
              latitud: <input type="text" class="gllpLatitude" value="20" disabled/>
            </div>
            <div class="informacionMapa">
              Longitud: <input type="text" class="gllpLongitude" value="20" disabled/>
            </div>
            <div class="botones_mapa">
            <input type="text" class="gllpZoom" value="15"/>
            <button type="button" class="btn btn-success" id="seleccionarMapa">Guardar</button>
            <button type="button" class="btn btn-default" id="cerrarMapa">Cancelar</button>
            </div>
            </fieldset>
          </div>
        </div>
        <div class="cargando">
            <div id="mensaje"></div>
        </div>
        <div class="jumbotron" id='filezone' style="height: 300px;  overflow: auto; display:none;"></div>
        <script type="text/javascript">
            redimencionarElementos();
            $('.cargando' ).dialog({ dialogClass: "no-close", autoOpen: false, modal: true});
            if($('.cargando' ).length>0){
              var eliminar = $('.cargando' ).get(0);
              $(eliminar).remove();
            }
            $('.mapaSelector' ).dialog({ dialogClass: "no-close", autoOpen: false, modal: true,width: "90%", sticky: true});
            CKEDITOR.replace( 'editor' );

            $('#radio_lugar').click(function() {
                $('.datosLugar').css('display','block');
            });
            $('#radio_noticia').click(function() {
                $('.datosLugar').css('display','none');
                $('#telefono').val('');
                $('#direccion').val('');
            });
            $('.publicar').click(function() {
              $('.cargando' ).children('#mensaje').html('Seguro que desea publicar');
              $('.cargando').dialog({buttons: { "Ok": {  text: 'Publicar', class: 'btn btn-link', click: function () {
                    var json = obtenerJsonGuardar();
                    ajaxPost("../../Controller/BorradorController.php?act=publicar","data="+json, "text", '');
                    $('.cargando').dialog( 'close' );
                    var _url = 'View/Administracion/admin.php';
                    localStorage.setItem('urlActual','View/Administracion/admin.php');
                    location.href = window.location.protocol+'//'+window.location.hostname+'/'+_url;
                  }},
                  "No": {  text: 'No publicar', class: 'btn btn-link', click: function () {
                    $('.cargando').dialog( 'close' );
                    //location.reload();
                  }}}
                });
                $('.cargando').dialog( 'open' );
            });
            $('.cerrar').click(function(){
              var _url = 'admin.php';
              localStorage.setItem('urlActual','admin.php');

              if($('#idBorrador').val()=='0' && hayCambios()){
                $('.cargando' ).children('#mensaje').html('Aun no se ha guardado la entrada, desea guardarla para después?');
                $('.cargando').dialog({buttons: { "Ok": {  text: 'Guardar', class: 'btn btn-link', click: function () {
                    var json = obtenerJsonGuardar();
                    ajaxPost("../../Controller/BorradorController.php?act=guardar","data="+json, "text", '');
                    $('.cargando').dialog( 'close' );
                    location.reload();
                  }},
                  "No": {  text: 'No guardar', class: 'btn btn-link', click: function () {
                    $('.cargando').dialog( 'close' );
                    location.reload();
                  }}}
                });
                $('.cargando').dialog( 'open' );
              }else{
                var json = obtenerJsonGuardar();
                ajaxPost("../../Controller/BorradorController.php?act=guardar","data="+json, "text", '');
                location.reload();
              }
            });
            $('#departamento').change(function(){
              var json = '{"codDepartamento":"'+ $('#departamento').val() +'"}';
              ajaxPost("../../Controller/MunicipioController.php?act=obtenerMunicipio","data="+json, "text", 'obtenerMunicipio');
            });
            $('#ubicacionMapa').click(function(){
              $(".gllpLatitude").val($('#municipio').children('option:selected').attr('latitud'));
              $(".gllpLongitude").val($('#municipio').children('option:selected').attr('longitud'));

              $('.mapaSelector' ).dialog("open");
              $(".gllpLatlonPicker").each(function() {
                $obj = $(document).gMapsLatLonPicker();

                $obj.params.strings.markerText = "Arrastre este marcador";

                $obj.params.displayError = function(message) {
                  console.error("MAPS ERROR: " + message); // instead of alert()
                };

                $obj.init( $(this) );
              });
            });
            $('#cerrarMapa').click(function() {
                $('.mapaSelector' ).dialog("close");
            });
            $('#seleccionarMapa').click(function() {
                $('#ubicacion').val($('.gllpLatitude').val()+','+$('.gllpLongitude').val());
                $('.mapaSelector' ).dialog("close");
            });
            $('.imagenCabecera >span').click(function() {
                $("#imagenCabecera").click();
            });
            $('body').delegate('.dz-image > img','click',function() {
                console.log('entra');
                $("#imagenCabecera").click();
            });
            var myDropzone = new Dropzone("#imagenCabecera", {
              url: "../../Controller/fileController.php",
              maxFilesize: 16,
              acceptedFiles: "image/*",
              addRemoveLinks: false,
              init: function () {
                this.on("addedfile", function() {
                  if (this.files[1]!=null){
                    this.removeFile(this.files[0]);
                  }
                });
                this.on("success", function (file, responseText) {
                  $('#imagenCabecera').children('.dz-preview').attr('nombreGuardado',responseText);
                  $('.imagenCabecera').children('span').remove();
                });
              }
            });

            function validarEntradasDatos(){
              var estado = ($('#titulo').val().length>0 && $('#ubicacion').val().length>0);
              return estado;
            }
            function hayCambios(){
              if($('#titulo').val().length>0){ return true; }
              var htmldata = CKEDITOR.instances.editor.document.getBody().getHtml();
              htmldata = JSON.stringify(htmldata);

              if(htmldata!=='<p><br></p>'){
                return true;
              }

              var categorias = '';
              $('.botonCategorias.active').each(function() {
                  categorias = ','+$(this).attr('codigoCategoria');
              });

              if(categorias.length>0){
                categorias = categorias.substring(1);
              }

              if(categorias.length>0){ return true; }

              imagenCabecera = '';
              if($('#imagenCabecera').children('.dz-preview').length){
                imagenCabecera = $('#imagenCabecera').children('.dz-preview').attr('nombreGuardado');
              }

              if(imagenCabecera.length>0){ return true; }

              if($('#descripcionImagen').val().length>0){ return true; }
              if($('#direccion').val().length>0){ return true; }
              if($('#ubicacion').val().length>0){ return true; }
            }
            function obtenerJsonGuardar(){
              var json = '{';
              var htmldata = CKEDITOR.instances.editor.document.getBody().getHtml();
              htmldata = JSON.stringify(htmldata);

              var tipoEntrada = '';
              $('#tipoPublicacion').children('label').children('input').each(function () {
                if($(this).prop('checked')){
                  tipoEntrada = $(this).val();
                }
              });

              var categorias = '';
              $('.botonCategorias.active').each(function() {
                  categorias = categorias+','+$(this).attr('codigoCategoria');
              });
              if(categorias.length>0){
                categorias = categorias.substring(1);
              }

              imagenCabecera = '';
              if($('#imagenCabecera').children('.dz-preview').length){
                imagenCabecera = $('#imagenCabecera').children('.dz-preview').attr('nombreGuardado');
              }

              json = json + '"departamento" : "'+$('#departamento').val()+'",'
                          + '"municipio" : "'+$('#municipio').val()+'",'
                          + '"idBorrador" : "'+$('#idBorrador').val()+'",'
                          + '"imagenCabecera" : "'+imagenCabecera+'",'
                          + '"descripcionImagen" : "'+$('#descripcionImagen').val()+'",'
                          + '"tipoEntrada" : "'+tipoEntrada+'",'
                          + '"telefono" : "'+$('#telefono').val()+'",'
                          + '"direccion" : "'+$('#direccion').val()+'",'
                          + '"categorias" : "'+categorias+'",'
                          + '"ubicacion" : "'+$('#ubicacion').val()+'",'
                          + '"titulo" : "'+$('#titulo').val()+'",'
                          + '"htmldata" : '+htmldata;


              json = json + '}';

              json = json.replace(/&nbsp;/g,' ');
              console.log(json);
              return json;
            }

            setTimeout(function(){
              var json = '{"codDepartamento":"'+ $('#departamento').val() +'"}';
              ajaxPost("../../Controller/MunicipioController.php?act=obtenerMunicipio","data="+json, "text", 'obtenerMunicipio');
            }, 1000);
        </script>