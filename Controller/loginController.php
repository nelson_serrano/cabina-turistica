<?php
    session_start();
    include_once '../Model/usuarioDao.php';
    $_SESSION['idOrden'] = '';
    $_SESSION['creationDate'] = '';
    $_SESSION['amount'] = '';
    $_SESSION['state'] = '';
    $json = $_POST['data'];
    $json = str_replace('\"','"',$json);
    $data = json_decode($json, true);
    $usuario = trim($data['user']);
    $contra = $data['password'];

    if(strcmp($usuario,'')!=0){

        $user = new Usuario;

        $result = $user->loginUsuario($usuario);

        if(sizeof($result)==1){
            $passWord = $result[0]['contrasena'];
            if(strcmp($passWord,$contra)==0) {
                $_SESSION['usuario'] = $result[0]['idUsuario'];
                $_SESSION['nombreUsuario'] = $result[0]['nombreUsuario'] . ' ' . $result[0]['apellidoUsuario'];
                $_SESSION['administrativo'] = $result[0]['administrativo'];
                $_SESSION['LAST_ACTIVITY'] = time();
                //header('Location: '.'../View/Menu/menuPrincipal.php');
                echo 'ok';
            } else {
                //$_SESSION['message'] = 'Password not match.';
                echo 'Password not match.';
                //header('Location: ../../');

            }
        } else {
            //$_SESSION['message'] = 'User not found.';
            echo 'User not found.';
            //header('Location: ../../');
        }
    } else {
        //$_SESSION['message'] = 'User field empty.';
        echo 'User field empty.';
        //header('Location: ../../');
    }
?>