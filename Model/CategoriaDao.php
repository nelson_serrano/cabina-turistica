<?php
class Categoria {
    function obtenerCategorias(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = " SELECT codigoCategoria, nombreCategoria, editable FROM CATEGORIAS ORDER BY nombreCategoria ASC";

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function obtenerCategoria($codCategoria){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = " SELECT codigoCategoria, nombreCategoria, editable FROM CATEGORIAS WHERE codigoCategoria='$codCategoria'";

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results[0];
    }

    function insertarCategorias_x_noticia($categoria, $idNoticia){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $update = "INSERT INTO CATEGORIA_X_NOTICIA(idNoticia, codigoCategoria) VALUES ($idNoticia,'$categoria')";

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();
    }

    function insertarCategorias_x_lugar($categoria,$idLugar){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $update = "INSERT INTO CATEGORIA_X_LUGAR(idLugarTuristico, codigoCategoria) VALUES ($idLugar,'$categoria')";

        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();
    }
}

?>