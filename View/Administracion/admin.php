<!DOCTYPE html>
<html>
  <?php
        session_start();
        header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');

        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
            // last request was more than 30 minutes ago
            session_unset();     // unset $_SESSION variable for the run-time
            session_destroy();   // destroy session data in storage
            header('Location: ../../login.php');  //redirect to login
        }
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

        if(isset($_SESSION['usuario'])){
            $user = $_SESSION['usuario'];
            $userName = $_SESSION['nombreUsuario'];
            $administrativo = $_SESSION['administrativo'];
        } else {
            header('Location: ../../login.php');
        }
        include_once "../../Model/usuarioDao.php";

    ?>
    <head>
        <link rel="icon" type="image/png" href="../../favicon.png?v=<?php echo md5_file('../../favicon.png') ?>"/>
        <title>Administracion Cabina Turistica</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="../../View/estilos/css/jquery-ui.css">
        <link rel="stylesheet" href="../../View/estilos/css/jquery-ui.theme.css">
        <link rel="stylesheet" href="../../View/estilos/css/bootstrap.css">
        <link rel="stylesheet" href="../../View/estilos/css/bootstrap-theme.css">
        <link rel="stylesheet" href="../../View/estilos/css/general.css">
        <link rel="stylesheet" href="../../View/estilos/css/nanoscroller.css">
        <link rel="stylesheet" href="../../View/js/Gritter/css/jquery.gritter.css">
    </head>
    <body>
        <nav class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                  <img src="../../favicon.png?v=<?php echo md5_file('../../favicon.png') ?>"></img>
                </a>
            </div>


            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="#" id="Administracion">Administracion</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="logout">Cerrar sesion</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        <div class="contenido">
            <div id="salida">
                <div class="col-sm-2 col-md-2 col-lg-2"></div>
                <div class="wrapper col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="espacioBoton">Para agregar nueva entrada o noticia: <button type="button" class="btn btn-warning editorTexto">Editor de texto</button></div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Entradas recientes
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Noticias recientes
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">

                            <!-- Noticias -->
                          </div>
                        </div>
                      </div>
                      <?php
                      if($administrativo==1){
                      ?>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Sección de usuarios
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">

                            <!-- usuarios -->
                            <button type="button" id="nuevoUsuario" class="btn btn-link" aria-label="Left Align">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar usuario
                            </button>
                            <div class="contenedorTabla">
                                <table class="table table-hover">
                                    <tr>
                                        <th>usuario</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Administrativo</th>
                                        <th>Acciones</th>
                                    </tr>
                                     <?php
                                    $usuario = new Usuario;
                                    $resultados = $usuario->getUsers();
                                    $cant = count($resultados);
                                    for($i=0;$i<$cant;$i++){
                                        echo '<tr idUsuario="'.$resultados[$i]['idUsuario'].'" contrasena="'.$resultados[$i]['contrasena'].'">
                                                <td>'.$resultados[$i]['idUsuario'].'</td>
                                                <td>'.$resultados[$i]['nombreUsuario'].'</td>
                                                <td>'.$resultados[$i]['apellidoUsuario'].'</td>
                                                <td>';
                                        if($resultados[$i]['administrativo']==1){
                                            echo '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';
                                        }else{
                                            echo '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>';
                                        }
                                        echo '</td><td>
                                                    <button type="button" class="btn btn-default edit" aria-label="Left Align" data-toggle="tooltip" data-placement="top" title="Editar usuario">
                                                      <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default delete" aria-label="Left Align" data-toggle="tooltip" data-placement="top" title="Eliminar usuario">
                                                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                    </button>
                                                </td>
                                            </tr>';
                                    }
                                    ?>
                                </table>
                            </div>

                          </div>
                        </div>
                      </div>
                      <?php
                      }
                      ?>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2"></div>
            </div>
        </div>
        <div class="cargando"><div id="mensaje"></div></div>
        <div class="cargandoPagina">
            <img src="../../View/imagenes/ajax-loader.gif"></img>
        </div>

        <script src="../../View/js/jquery-2.2.3.js"></script>
        <script src="../../View/js/jquery-ui.js"></script>
        <script src="../../View/js/bootstrap.js"></script>
        <script src="../../View/js/general.js"></script>
        <script src="../../View/js/jquery.nanoscroller.min.js"></script>
        <script src="../../View/js/ckeditor/ckeditor.js"></script>
        <script src="https://maps.google.com/maps/api/js?sensor=false"></script>
        <script src="../../View/js/jquery-gmaps-latlon-picker.js"></script>
        <script src="../../View/js/dropzone.js"></script>
        <script src="../../View/js/sha3.js"></script>
        <script src="../../View/js/Gritter/js/jquery.gritter.min.js"></script>
        <script src="../../View/js/qrcode.min.js"></script>
        <script type="text/javascript">
          $('.cargando' ).dialog({ dialogClass: "no-close", autoOpen: false, modal: true, width: 500});
          verificarPaginaActiva();
          $.gMapsLatLonPickerNoAutoInit = 1;
          $( window ).resize(function() {
              clearTimeout(window.resizedFinished);
              window.resizedFinished = setTimeout(function(){
                  redimencionarElementos();
              }, 150);
          });
          $('.editorTexto').click(function(event) {
              event.preventDefault();
              var _url = 'editorTexto.php';
              localStorage.setItem('urlActual','editorTexto.php');
              cambiarDePagina(_url);
          });

          $('body').delegate('#idUsuario','focusin',function() {
            $.gritter.add({
      				title: 'Aviso',
      				text: 'El correo es un campo unico que se usara para identificar al usuario, no se podrá cambiar.'
      			});

      			return false;
          });
          $('body').delegate('#contra','focusin',function() {
              if($('#idUsuario:disabled').length>0){
                $.gritter.add({
          				title: '¡Cuidado con la contraseña!',
          				text: 'Si la cambias aquí, se le cambiara al usuario... dejala en blanco y no pasará nada con ella'
          			});
          			return false;
              }
          });
          $('body').delegate('#nuevoUsuario','click',function(){
            var html = '<form class="form-horizontal">'
                        +'<div class="form-group">'
                          +'<label for="idUsuario" class="col-sm-3 control-label">Email</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="email" class="form-control" id="idUsuario" placeholder="Email">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="nombreUsuario" class="col-sm-3 control-label">Nombre</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="text" class="form-control" id="nombreUsuario" placeholder="Nombre">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="apellidoUsuario" class="col-sm-3 control-label">Apellido</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="text" class="form-control" id="apellidoUsuario" placeholder="Apellido">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="contra" class="col-sm-3 control-label">Contrase&ntilde;a</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="password" class="form-control" id="contra" placeholder="Contrase&ntilde;a">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="administrativo" class="col-sm-3 control-label">Administrativo</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="checkbox" id="administrativo">'
                          +'</div>'
                        +'</div>'
                      +'</form>';
            $('.cargando').children('#mensaje').html(html);
            $('.cargando').dialog({buttons: {
               "Ok": {  text: 'Guardar', class: 'btn btn-primary', click: function () {
                 var hash = CryptoJS.SHA3($('#contra').val());

                  var json = '{"idUsuario":"'+$('#idUsuario').val()+'",'
                        + '"nombreUsuario":"'+$('#nombreUsuario').val()+'",'
                        + '"apellidoUsuario":"'+$('#apellidoUsuario').val()+'",'
                        + '"contra":"'+hash.toString(CryptoJS.enc.Hex)+'",'
                        + '"administrativo":"'+($('#administrativo').is(':checked')?1:0)+'"}';

                  ajaxPost_Sinc("../../Controller/usuarioController.php?act=insertar","data="+json, "text", '');
                  $('.cargando').dialog( 'close' );
                  location.reload();
               }},
              "cancel": {  text: 'Cancelar', class: 'btn btn-default', click: function () {
                  $('.cargando').dialog( 'close' );
               }}}});
            $('.cargando').dialog('open');
          });
          $('body').delegate('.edit','click',function(){
            var html = '<form class="form-horizontal">'
                        +'<div class="form-group">'
                          +'<label for="idUsuario" class="col-sm-3 control-label">Email</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="email" class="form-control" id="idUsuario" placeholder="Email" value="'+$($(this).parent().parent().children().get(0)).html()+'" disabled>'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="nombreUsuario" class="col-sm-3 control-label">Nombre</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="text" class="form-control" id="nombreUsuario" value="'+$($(this).parent().parent().children().get(1)).html()+'" placeholder="Nombre">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="apellidoUsuario" class="col-sm-3 control-label">Apellido</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="text" class="form-control" id="apellidoUsuario" value="'+$($(this).parent().parent().children().get(2)).html()+'" placeholder="Apellido">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="contra" class="col-sm-3 control-label">Contrase&ntilde;a</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="password" class="form-control" id="contra" placeholder="Contrase&ntilde;a">'
                          +'</div>'
                        +'</div>'
                        +'<div class="form-group">'
                          +'<label for="administrativo" class="col-sm-3 control-label">Administrativo</label>'
                          +'<div class="col-sm-9">'
                            +'<input type="checkbox" id="administrativo">'
                          +'</div>'
                        +'</div>'
                      +'</form>';
            $('.cargando').children('#mensaje').html(html);
            $('.cargando').dialog({buttons: {
               "Ok": {  text: 'Guardar', class: 'btn btn-primary', click: function () {
                 var hash = '';
                 if($('#contra').val().length>0){
                   hash = CryptoJS.SHA3($('#contra').val());
                   hash = hash.toString(CryptoJS.enc.Hex);
                 }

                var json = '{"idUsuario":"'+$('#idUsuario').val()+'",'
                        + '"nombreUsuario":"'+$('#nombreUsuario').val()+'",'
                        + '"apellidoUsuario":"'+$('#apellidoUsuario').val()+'",'
                        + '"contra":"'+hash+'",'
                        + '"administrativo":"'+($('#administrativo').is(':checked')?1:0)+'"}';

                ajaxPost_Sinc("../../Controller/usuarioController.php?act=actualizar","data="+json, "text", '');
                $('.cargando').dialog( 'close' );
                location.reload();
               }},
              "cancel": {  text: 'Cancelar', class: 'btn btn-default', click: function () {
                  $('.cargando').dialog( 'close' );
               }}}});
            $('.cargando').dialog('open');
            if($($(this).parent().parent().children().get(3)).children().hasClass('glyphicon-star')){
              $('#administrativo').prop('checked', true);
            }
          });
          $('body').delegate('.delete','click',function() {
            var idUsuario = $(this).parent().parent().attr('idusuario');
            $('.cargando').children('#mensaje').html('Esta seguro que desea eliminar el usuario');
            $('.cargando').dialog({buttons: {
               "Ok": {  text: 'Eliminar', class: 'btn btn-danger', click: function () {
                  var json = '{"idUsuario":"'+idUsuario+'"}';
                  ajaxPost_Sinc("../../Controller/usuarioController.php?act=eliminar","data="+json, "text", '');
                  $('.cargando').dialog( 'close' );
                  location.reload();
               }},
              "cancel": {  text: 'Cancelar', class: 'btn btn-default', click: function () {
                  $('.cargando').dialog( 'close' );
               }}}});
               $('.cargando').dialog('open');
          });
          $('#logout').click(function(event){
            event.preventDefault();
            location.href = location.protocol+'//'+location.host+'/login.php';
          });
          $('#Administracion').click(function(event){
            event.preventDefault();
            localStorage.setItem('urlActual','');
            location.reload();
          });

          setTimeout(function(){
            var urlActual = localStorage.getItem('urlActual');
            if(urlActual.indexOf('editorTexto')<0){
              ajaxPost("../../Controller/lugaresInteresController.php?act=obtenerLugaresDescripcion","data=0", "text", 'obtenerLugaresAdmin');
            }
          },250);
          setTimeout(function(){
            if(urlActual.indexOf('editorTexto')<0){
              ajaxPost("../../Controller/NoticiasController.php?act=obtenerNoticias","data=0", "text", 'obtenerNoticiasAdmin');
            }
          },500);
        </script>
    </body>
</html>