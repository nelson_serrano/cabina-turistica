<div class="barraTitulo">
    <div class="botonAtras"></div>
    <?php
    include_once '../../Model/LugarDao.php';
    $codCategoria = $_GET['categoria'];

    $cat = new Lugar;
    $result = $cat->listaLugaresCategoria($codCategoria);
    ?>
    <h1><?php echo $result[0]['nombreCategoria']; ?></h1>
</div>

<div class="col-sm-2 col-md-2 col-lg-2"></div>
<div class="wrapper col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <div class="list-group">
      <?php
        foreach ($result as $lugar) {
          if(strlen($lugar['nombreLugar'])>0){
            echo '<a href="#" class="list-group-item descripcionLugar" idLugarTuristico='.$lugar['idLugarTuristico'].'>
                  <div id="iconoPrueba" class="categoriasImagenes" style="background-image: url('.$lugar['imgTipoLugar'].')"></div>
                  <div class="titulo_categoria">'.$lugar['nombreLugar'].'</div>
                  <div class="contenido_categoria">'.$lugar['descripcionLugar'].'</div>
                </a>';
          }
        }

      ?>
    </div>
</div>
<div class="col-sm-2 col-md-2 col-lg-2"></div>