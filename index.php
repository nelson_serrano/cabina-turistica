<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" type="image/png" href="favicon.png?v=<?php echo md5_file('favicon.png') ?>"/>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cabina Turistica</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="View/estilos/css/jquery-ui.css">
        <link rel="stylesheet" href="View/estilos/css/jquery-ui.theme.css">
        <link rel="stylesheet" href="View/estilos/css/bootstrap.css">
        <link rel="stylesheet" href="View/estilos/css/bootstrap-theme.css">
        <link rel="stylesheet" href="View/estilos/css/general.css">
        <link rel="stylesheet" href="View/estilos/css/nanoscroller.css">
    </head>
    <body>
        <nav class="navbar navbar-default">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="favicon.png?v=<?php echo md5_file('favicon.png') ?>"></img>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="#" id="lugaresDeInteres">Lugares de interés</a></li>
                <li><a href="#" id="fiestasPatronales">Fiestas patronales</a></li>
                <li><a href="#" id="lugaresDeComida">Lugares de comida</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <div class="contenido">
            <div id="salida">
                <div class="tituloBienvenida">
                    <h1>¡Bienvenidos al municipio de Suchitoto!</h1>
                </div>
                <div class="descripcion visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <h1>Descripcion</h1>
                    <div id="mapholder" class="mapaLocalizacion"></div>
                    <div class="descripcionMunicipioPrincipal nano">
                        <div class="nano-content">
                            <p>
                                Suchitoto es un municipio del departamento de Cuscatlán, El Salvador. Su territorio ha sido habitado desde la época precolombina, y también fue el sitio donde se fundó la villa de San Salvador en 1528, que tuvo una breve existencia. A partir de la finalización de la guerra civil salvadoreña, la localidad ha prosperado hasta convertirse en uno de los más importantes sitios turísticos de este país. Según el censo oficial de 2007, tiene una población de 24,786 habitantes.
                            </p>
                            <p>
                                <h3>Historia</h3>
                                Época precolombina y colonización española
                                La población es de origen precolombino, y sus moradores pertenecían a la etnia nahua. Ya era un sitio densamente poblado a la llegada de los conquistadores españoles, quienes refundaron la villa de San Salvador a unos 12 km de este lugar por Diego de Alvarado en el valle de la Bermuda en abril de 1528, mediante disposición del Teniente de Gobernador y Capitán General don Jorge de Alvarado. La fundación original había tenido lugar en 1525 en un lugar no determinado.
                            </p>
                            <p>
                                La villa, puesta bajo la advocación de la Santísima Trinidad, tuvo una difícil existencia. Aparte del asedio de los nativos, los aldeanos enfrentaron la amenaza de Martín de Estete, un enviado de Pedrarias Dávila que pretendía que le reconocieran como su Teniente Gobernador y Capitán General. Hacia 1539, los pobladores empezaron a trasladarse al valle de las Hamacas, sitio actual de la ciudad de San Salvador, por lo que el poblado de la Bermuda quedó abandonado. La autorización oficial del traslado fue realizada mediante resolución de la Real Audiencia de los Confines en 1545. Para el año 1550 Suchitoto tenía unos 600 habitantes.
                            </p>
                            <p>
                                Debido a su posición privilegiada, la localidad era cabecera parroquial de un extenso territorio del obispado guatemalteco. Para 1770, según Pedro Cortés y Larraz, existían unos 43 "indios tributarios", por lo que se estimaba su población en 215 personas. Además, como cabeza de curato, ejercía jurisdicción en los pueblos de Jutiapa y Tenancingo. Precisamente, el prócer centroamericano Vicente Aguilar fue uno de sus párrocos. Por otro lado, cuando comenzó el auge de la fabricación de añil en la región en el siglo XVII, fue uno de sus centros de producción más importante.2 El año 1786, Suchitoto ingresó al Partido de San Salvador de la Intendencia de San Salvador. De acuerdo al Intendente Antonio Gutiérrez y Ulloa, habitaban en el lugar 911 personas para 1807.
                            </p>
                            <p>
                                <h3>Época republicana</h3>
                                Ya en la época republicana, pasó a formar parte del departamento de San Salvador, una vez fue constituido dicho departamento el 12 de junio de 1824. Para 1835, mediante decreto del Jefe Supremo Nicolás Espinoza, se constituyó como cabecera del departamento de Cuscatlán, y era también parte del distrito de Suchitoto. La localidad adquirió el título de villa el 22 de marzo de 1836. Por otro lado, en medio de los conflictos que asolaron a la República Federal de Centro América, en Suchitoto se encontraba el General Francisco Morazán en el año 1839, quien trataba de impedir una avanzada de Francisco Ferrera. También fue allí donde recibió a los enviados del insurrecto Pedro León Velásquez, acantonado en San Salvador, quien le demandó su capitulación, pues, de no hacerlo, ejecutaría a su esposa Josefa Lastiri, su hijo natural Francisco y la recién nacida Adela junto a otros de sus familiares. Morazán respondió que atacaría esa ciudad aún "pasando sobre los cadáveres" de sus hijos. Al final, Morazán ocupó San Salvador y perdonó la vida a Velásquez.
                            </p>
                            <p>
                                Durante la administración del Presidente Gerardo Barrios, Suchitoto alcanzó el título de ciudad por Decreto Ejecutivo del 15 de julio de 1858, sancionada por las Cámaras Legislativas el 11 de febrero de 1859. Para este mismo año, contaba con una población de 6,455 habitantes y entre sus vecinos habían 4 abogados, 6 médicos, un agrimensor, 4 pintores y un escultor, así como 153 músicos. Además, un informe municipal, establecía:
                            </p>
                            <p>
                                <blockquote>
                                    En el recinto de esta Ciudad se encuentran 265 casas de teja, de buena construcción y en algunas de ellas, no sólo se han consultado la seguridad y comodidades, sino también el gusto en cuanto lo permite el país. Fuera de este número hay un cabildo con sus correspondientes oficinas interiores, sus cárceles de hombres y mujeres con la debida separación unas de otras, y con sus potreros bastante cómodos para el desahogo de los reos...también tiene una campana de 115 libras para los usos de costumbre...se cuentan 308 casas de paja con corredores de teja, construidas sobre paredes de bahareque las más, y 347 enteramente pajizas, de la misma construcción, siendo por todas 922 casas fuera de las iglesias.
                                </blockquote>
                                Suchitoto dejó de ser cabecera del departamento de Cuscatlán por Ley del 12 de noviembre de 1861, en favor de Cojutepeque. Para el año 1890 la localidad tenía 13.820 habitantes, y la población estaba dividida, según el historiador Guillermo Dawson:
                            </p>
                            <p>
                                <blockquote>
                                    ...en cuatro barrios, llamados San José, Santa Lucía, El Calvario y Concepción. Sus calles, empedradas la mayor parte, son bastante rectas. Tiene dos iglesias, de las cuales la parroquia es una hermosa y sólida construcción. de mampostería. Sus otros edificios públicos son el cabildo, cárceles y casas de escuela. Hay muy buenas y amplias casas particulares.
                                </blockquote>
                            </p>
                            <p>
                                <h3>Siglos XX y XXI</h3>
                                Para el año 1958, fue celebrada con mucha solemnidad el centenario de la obtención del título de ciudad. La ocasión contó con la asistencia del Presidente José María Lemus, y también fue estrenado el escudo de la localidad. Para ese tiempo, Suchitoto era un importante centro comercial del área, por ser una de las rutas que permitían arribar a Chalatenango; además, existía un notable desarrollo de la ganadería y la agricultura. Su decaimiento inició con la construcción de la Central Hidroeléctrica Cerrón Grande, que provocó el surgimiento de un embalse en 1973 —conocido posteriormente como Lago de Suchitlán— y con ello el aislamiento de la población y la inundación de campos fértiles; asimismo, hubo pérdida de restos arqueológicos.2
                            </p>
                            <p>
                                Durante los años 1980, Suchitoto sufrió el embate de la Guerra Civil de El Salvador. Aparte de la destrucción de sus edificios, los pobladores emigraron de la localidad, por lo que terminó casi abandonada. De acuerdo a un testimonio, de los 10,000 habitantes que tenía Suchitoto apenas permanecieron en el lugar unas 50 familias. En 1992, tras la firma de los Acuerdos de Paz de Chapultepec, inició su proceso de reconstrucción y el retorno de sus habitantes. 8 Desde entonces, Suchitoto es uno de los emblemas turísticos de El Salvador, y en su crecimiento ha sido meritoria la labor del cineasta Alejandro Cotto.
                            <p>
                                <h3>Geografía y toponimia</h3>
                                El municipio tiene una extensión de 329.2 km², y la cabecera una altitud de 388 msnm. Su área comprende 28 cantones. Posee áreas naturales entre las que destacan: Colima (nor-poniente), el cerro Guazapa (sur-poniente), cerro Tecomatepe (sur), parte del bosque de Cinquera (oriente), y el bosque Corozal-El Platanar y embalse Cerrón Grande o Lago Suchitlán. La superficie del territorio se encuentra conformada por un 41.7% de planicies, por lo que se clasifica como "medio-llano".
                            </p>
                            <p>
                                <h4>Toponimia</h4>
                                El topónimo Suchitoto, probablemente Suchitotoc, según Pedro Geoffroy Rivas, significa "Lugar del pájaro flor" (shuchit: flor; tutut: pájaro; C: en, lugar); o también: "como pájaro-flor" o "lugar de pájaros y flores" (súchit, shúshil: flor, y toto, tutut: pájaro). Asimismo, la evolución gráfica del topónimo ha sido: Suchitot (1548), Santa Lucía Suchitoto (1770), y Suchitoto (1807).
                            </p>
                            <p>
                                <h3>Símbolos</h3>
                                <h4>Escudo</h4>
                                El Escudo de Suchitoto se encuentra dividido en cuatro cuarteles, y su interpretación es la siguiente:

                                <blockquote>
                                    Los colores simbolizan: Azul, serenidad, reflexión, quietud, estudio; cuanto piensa y siente, y el pájaro vuela serenamente sobre las flores; el Rojo en cambio simboliza los momentos cruciales que pueden ser guerra, tragedia, gloria, poder, potestad, el pájaro permanece sereno, le es indiferente la tragedia o la gloria, pero sin perder el número romántico y poético de las flores.</br></br>
                                    La “S” de plata con deltas en los extremos, es el río Lempa que forma la “S” inicial de Suchitoto, que encierra en su simbología del “pájaro-flor”...La montaña es el Volcán de Guazapa, que simboliza la tierra Americana (en este caso Suchitoto). El Sol de Oro, simboliza el sol de la Cultura iluminando las tierras nuevas, ambos se encuentran en un cuartel rojo, porque la conquista fue violenta. En el cuarto cuartel, en azul tenue, se plasma la iglesia de Suchitoto como símbolo de las virtudes cristianas sobre un fondo de paz y hermosura. El lema “Primus in vita et in virtutivos vital” significa “Primero en la vida y las virtudes de la vida”, hace alusión a la tradición comúnmente aceptada que aquí en Suchitoto de dio el primer mestizaje. La corona, es una muralla de oro con Parteluces y Matacanes; encierra las tres virtudes teologales que son fe, Esperanza y Caridad, y simboliza el título de ciudad obtenido el 15 de Julio de 1858.</br></br>
                                    El emblema fue elaborado por el poeta y escritor Manuel José Arce y Valladares, con la colaboración de Alejandro Cotto, y presentado en el año 1958 en ocasión del centenario de la ciudad.
                                </blockquote>
                            </p>
                            <p>
                                <h4>Himno</h4>
                                El himno de la ciudad también fue compuesto por el cineasta Alejandro Cotto, y sus primeras estrofas son las siguientes:

                                <blockquote>
                                    Hoy nuestras voces cantan un himno. Himno de alegría devoción y fe.<br/>
                                    Suchitoto flor y vuelo<br/>
                                    Fuente de nuestro amor<br/>
                                    Faro del corazón<br/>
                                    Nuevo destino te prometemos<br/>
                                    Nuestro anhelo inspira tu blasón
                                </blockquote>
                                <h3>Cultura y sociedad</h3>
                                <h4>Fiestas religiosas y populares</h4>
                                Las fiestas patronales se celebran en el mes de diciembre en honor a Santa Lucía. Para esa ocasión, en Suchitoto se desarrollan diversas manifestaciones religiosas y populares como procesión católica, presentaciones musicales, feria ganadera, quema de pólvora, elección de la reina de las fiestas, degustación de comida tradicional, bailes y ferias comerciales.

                                Otros eventos incluyen el aniversario del otorgamiento del título de ciudad el 15 de julio, con actos oficiales y desfiles escolares; así como el festival del maíz en el mes de agosto, el cual inició después de la celebración de los Acuerdos de Paz de Chapultepec, y el cual incluye la promoción de artesanías y alimentos a partir de ese grano; también se lleva a cabo la feria del añil en el mes de noviembre; y conmemoraciones del retorno de los pobladores a sus comunidades tras la finalización del conflicto armado como en Copapayo y El Chagüitón, entre otros. Asimismo, tienen realce la Semana Santa, el Día de la Cruz del 3 de mayo, y la conmemoración de la Independencia de Centroamérica el 15 de septiembre.

                                <h4>Festival Permanente de Arte y Cultura</h4>
                                Establecido por iniciativa de Alejandro Cotto el año de 1991, el festival tiene lugar en el mes de febrero en el Teatro de las Ruinas, y cuenta con la participación de artistas nacionales e internacionales, que muestran su arte en orquestas sinfónicas, música, ópera, danza, teatro, lecturas de poemas, o cantos gregorianos. Además, a lo largo del año y por lo menos una vez al mes, existen eventos culturales en diversos puntos de la ciudad.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="noticias_wrapper visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-4 col-lg-4">
                </div>
            </div>
        </div>

        <div class="cargando"><div id="mensaje"></div></div>
        <div class="cargandoPagina">
            <img src="View/imagenes/ajax-loader.gif"></img>
        </div>

        <script src="View/js/jquery-2.2.3.js"></script>
        <script src="View/js/jquery-ui.js"></script>
        <script src="View/js/bootstrap.js"></script>
        <script src="https://maps.google.com/maps/api/js?sensor=false"></script>
        <script src="View/js/general.js"></script>
        <script src="View/js/jquery.nanoscroller.min.js"></script>
        <script src="View/js/qrcode.min.js"></script>

        <script type="text/javascript">
            verificarPaginaActiva();
            $('.cargando' ).dialog({ dialogClass: "no-close", autoOpen: false, modal: true});

            $( window ).resize(function() {
                clearTimeout(window.resizedFinished);
                window.resizedFinished = setTimeout(function(){
                    redimencionarElementos();
                }, 150);
            });
            setInterval(function(){
                ajaxPost("../../Controller/NoticiasController.php?act=obtenerNoticias","data=0", "text", 'obtenerNoticias');
            }, 60000);
            ajaxPost("../../Controller/NoticiasController.php?act=obtenerNoticias","data=0", "text", 'obtenerNoticias');
        </script>
    </body>
</html>