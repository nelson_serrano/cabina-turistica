<?php
header('Content-Type: text/html; charset=utf-8' );
class Noticia {
    function publicar($data,$user){
        $idBorrador = $data['idBorrador'];

        $ultimoPrev = $this->obtenerUltimo();

        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $latLon = explode(',',$data['ubicacion']);
        $latitudLugar = $latLon[0];
        $longitudLugar = $latLon[1];

        $update = "INSERT INTO NOTICIA(idMunicipio, tituloNoticia,imagenCabecera,
                     descriptionImagenCabecera, contenidoNoticia)
                    VALUES ('".$data['municipio']."','".$data['titulo']."','".$data['imagenCabecera']
                    ."','".$data['descripcionImagen']."','".mysql_escape_string($data['htmldata'])."')";


        //echo $update.'   /   ';
        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();
        $ultimoDesp = $this->obtenerUltimo();
        if($ultimoPrev!=$ultimoDesp){
            $this->asociarCategorias($data['categorias'],$ultimoDesp);
        }

        //muevo la imagen a la carpeta donde deberia de estar
        if(strlen($data['imagenCabecera'])>0){
            rename("../View/imagenes/subidasDeUsuarios/temp/".$data['imagenCabecera'],"../View/imagenes/subidasDeUsuarios/".$data['imagenCabecera']);
        }

    }

    function asociarCategorias($categorias,$ultimoDesp){
        include 'CategoriaDao.php';
        $categoriasSeparadas = explode(',',$categorias);
        //var_dump($categoriasSeparadas);
        $cat = new Categoria;
        foreach($categoriasSeparadas as $categoria){
            //echo $categoria;
            $cat->insertarCategorias_x_noticia($categoria,$ultimoDesp);
        }
    }

    function obtenerUltimo(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT max(idNoticia) max FROM NOTICIA";
        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results= $row['max'];
            }
        }
        $db->close();

        if (isset($results))
            return $results;
        else
            return 0;
    }

    function obtenerUltimasNoticias(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT idNoticia, idMunicipio, tituloNoticia, imagenCabecera, descriptionImagenCabecera, contenidoNoticia FROM NOTICIA ORDER BY idNoticia DESC LIMIT 0,30";

        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[]= $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function obtenerNoticia($idNoticia){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $consulta = "SELECT idNoticia, idMunicipio, tituloNoticia, imagenCabecera, descriptionImagenCabecera, contenidoNoticia
                    FROM NOTICIA WHERE idNoticia = ".$idNoticia;
        //echo $consulta;

        $result = $db->query($consulta);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results= $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
        else
            return 0;
    }
}
?>