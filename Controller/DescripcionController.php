<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    include_once '../Model/DescripcionDao.php';


    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if($action  == 'getDescripcion'){
             $json = $_POST['data'];

            $json = str_replace('\"','',$json);
            $jsonDecode = json_decode($json, true);

            $nombreLugar = $jsonDecode['nombreLugar'];

            $des = new descripcion;
            $resultado = $des->getDescripcion($nombreLugar);

            echo '<div class="barraTitulo">
                        <div class="botonAtras"></div>
                        <h1 id="nbrLugarTur">'.utf8_encode($resultado[0]).'</h1>
                    </div>
                    <ol class="breadcrumb">
                      <li><a href="#" id="inicio">Inicio</a></li>
                      <li><a href="#" id="categorias">Categorias</a></li>
                      <li><a href="#" id="lugares">Lugares</a></li>
                      <li class="active">Descripcion</li>
                    </ol>

                    <center>
                    <div class="contenido_desc">
                    <div id="contenidoDesc">
                    <div class="separadoresDesc visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
                    <div class= "descImg visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <center><div class="imgDescripcion img-thumbnail" id="bannerImg" style="background-image: url('.$resultado[3].');"></div></center>
                    <div class="desc" id="imgDesc">'.utf8_encode($resultado[4]).'</div>
                    </div>
                    <div class="separadoresDesc visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
                    <div class="space-2"></div>
                    <div class="contenido">
                    <p>'.utf8_encode($resultado[5]).'</p>
                    </div>';
        }
    }
?>