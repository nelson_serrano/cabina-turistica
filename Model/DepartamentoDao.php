<?php
header('Content-Type: text/html; charset=utf-8' );
class Departamento {
    function obtenerDepartamentos(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $consulta = " SELECT codDepartamento, nombreDepartamento, latitudDepartamento, longitudDepartamento
                        FROM DEPARTAMENTO ORDER BY nombreDepartamento ASC";

        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }
}
?>