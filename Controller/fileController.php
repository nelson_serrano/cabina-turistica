<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
        // last request was more than 30 minutes ago
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        header('Location: ../../');  //redirect to login
    }
    $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    //echo var_dump($_POST);
    if(isset($_SESSION['usuario'])){
        $user = $_SESSION['usuario'];
        $_SESSION['usuario'] = $user;
    }

    if (!empty($_FILES)) {

        $fileName = $_FILES['file']['name'];
        $tmpName  = $_FILES['file']['tmp_name'];
        $fileSize = $_FILES['file']['size'];
        $fileType = $_FILES['file']['type'];

        $temp = explode(".", $_FILES["file"]["name"]);

        $newfilename = $user.'-cover-'. round(microtime(true)) . '.' . end($temp);
        $files = glob('../View/imagenes/subidasDeUsuarios/temp/'.$user .'-cover-*'); // get all file names
        /*foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }*/
        // si ya se sabe el id al que se le va a asociar
        move_uploaded_file($_FILES["file"]["tmp_name"], "../View/imagenes/subidasDeUsuarios/temp/" . $newfilename);
        echo $newfilename;

    } else {
        echo '$_FILES no seteado';
        var_dump($_FILES);
    }
?>