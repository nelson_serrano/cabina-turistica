<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    include_once '../Model/lugaresInteresDAO.php';
    include_once '../Model/LugarDao.php';
    header("Content-type: text/html; charset=UTF-8");

    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if($action == 'getLugaresInteres'){
            $lugar = new Lugar();
            $resultado = $lugar -> getLugaresDesc();

            foreach($resultado as $lugTur){
                echo '<a href="#" class="list-group-item descripcionLugar" idLugarTuristico="'.$lugTur['idLugarTuristico'].'">
                            <div id="iconoPrueba" class="categoriasImagenes" style="background-image: url('.$lugTur['imgTipoLugar'].')"></div>
                            <div class="titulo_categoria">'.$lugTur['nombreLugar'].'</div>
                            <div class="contenido_categoria">'.$lugTur['descripcionLugar'].'</div>
                          <p id="codTipLugar" style="visibility: hidden;">'.$lugTur['idLugarTuristico'].'</p>
                          </a>';
            }
        }
        elseif($action=='obtenerLugaresDescripcion'){
            $lugar = new Lugar();
            $resultado = $lugar -> getLugaresDesc();

            foreach($resultado as $lugTur){
                echo '<a href="#" class="list-group-item descripcionLugar" idLugarTuristico="'.$lugTur['idLugarTuristico'].'">
                            <div id="iconoPrueba" class="categoriasImagenes" style="background-image: url('.$lugTur['imgTipoLugar'].')"></div>
                            <div class="titulo_categoria">'.$lugTur['nombreLugar'].'</div>
                            <div class="contenido_categoria">'.$lugTur['descripcionLugar'].'</div>
                          <p id="codTipLugar" style="visibility: hidden;">'.$lugTur['idLugarTuristico'].'</p>
                          </a>';
            }
        }
    }
?>