<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    if(isset($_SESSION['usuario'])){
        $user = $_SESSION['usuario'];
        $_SESSION['usuario'] = $user;
    }

    include_once '../Model/BorradorDao.php';
    include_once '../Model/LugarDao.php';
    include_once '../Model/NoticiaDao.php';

    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if ($action=='guardar'){
            $json = $_POST['data'];

            $data = json_decode($json, true);

            $borrador = new Borrador;
            $result = $borrador->guardarBorrador($data,$user);
            echo $result;
        }
        elseif ($action=='vistaPrevia'){
            $json = $_POST['data'];

            $json = str_replace('\"','"',$json);
            $data = json_decode($json, true);

            $borrador = new Borrador;

            $result = $borrador->guardarBorrador($data,$user);

            echo '{ "resultado":"'.$result.'","titulo":"'.$data['titulo'].'","idBorrador":"'.$data['idBorrador'].'"}';
        }elseif($action=='publicar'){
            $json = $_POST['data'];
            //echo $json.'<br/>\n';
            $data = json_decode($json, true);
            //var_dump($data);
            if($data['tipoEntrada']=='noticia'){
                $noticia = new Noticia;
                $noticia->publicar($data,$user);
            }else{
                $lugar = new Lugar;
                $lugar->publicar($data,$user);
            }

            $borrador = new Borrador;
            $borrador->borrarBorrador($data['idBorrador']);
        }
    }
?>