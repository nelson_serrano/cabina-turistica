<!DOCTYPE html>
<?php
    session_start();
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
    header('Access-Control-Allow-Origin: *');
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="icon" type="image/png" href="favicon.png?v=<?php echo md5_file('favicon.png') ?>"/>
        <meta charset="UTF-8">
        <title>Cabina Turistica</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="View/estilos/css/jquery-ui.css">
        <link rel="stylesheet" href="View/estilos/css/jquery-ui.theme.css">
        <link rel="stylesheet" href="View/estilos/css/bootstrap.css">
        <link rel="stylesheet" href="View/estilos/css/bootstrap-theme.css">
        <link rel="stylesheet" href="View/estilos/css/general.css">
        <link rel="stylesheet" href="View/estilos/css/nanoscroller.css">
        <style type="text/css">
            .contenidoCentral {
                margin-left: 40%;
                margin-right: 40%;
                margin-top: 5%;
            }
            .logo{
                height: 200px;
                margin: auto;
            }
            @media (max-width: 770px) {
                .contenidoCentral {
                    margin-left: 30%;
                    margin-right: 30%;
                }
            }
            @media (max-width: 430px) {
                .contenidoCentral {
                    margin-left: 20%;
                    margin-right: 20%;
                }
            }
            @media (max-width: 375px) {
                .contenidoCentral {
                    margin-left: 10%;
                    margin-right: 10%;
                }
            }
            @media (max-width: 320px) {
                .contenidoCentral {
                    margin-left: 0%;
                    margin-right: 0%;
                }
            }
        </style>
    </head>
    <body>
        <div class="contenidoCentral">
            <img class="logo img-responsive" src="favicon.png"></img>
            <form class="form-horizontal" role="form" method="post">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12">
                        <input type="email" class="form-control mailType" id="User" name="User" placeholder="Email"
                            style="text-align: center;border: 0px solid #cccccc;border-bottom-width: 1px;box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075);border-radius: 0px;"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12" style="margin-top: -10px;">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                            style="text-align: center;border: 0;box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075);"  autocomplete="on">
                    </div>
                </div>
                <div class="contenidoBoton" style="text-align: center;top: 38px;">
                    <input id="submit" type="button" class="btn btn-link loginButton" value="Sign in"></button>
                </div>
            </form>
        </div>

        <div class="cargando">
            <div id="mensaje"></div>
        </div>

         <script src="View/js/jquery-2.2.3.js"></script>
        <script src="View/js/jquery-ui.js"></script>
        <script src="View/js/bootstrap.js"></script>
        <script src="View/js/general.js"></script>
        <script src="View/js/sha3.js"></script>

        <script type="text/javascript">
            localStorage.clear();
            $('.cargando' ).dialog({ dialogClass: "no-close", autoOpen: false, modal: true,width: "300px", sticky: true});

            function login() {
                var hash = CryptoJS.SHA3($('#password').val());

                $('.cargando').dialog({buttons: { }});

                //$('.cargando').dialog( "open" );
                var json = '{"user":"'
                            + $('#User').val()
                            + '","password":"'
                            + hash.toString(CryptoJS.enc.Hex)
                            + '"}';

                console.log(json);

                ajaxPost_Sinc("Controller/loginController.php"
                            ,"data="+json, "text", 'login');
            }

            $('#submit').click(function(event) {
                event.preventDefault();
                login();
            });

            $('input').keypress(function(event){
                if (event.keyCode === 13){
                    event.preventDefault();
                    login();
                }
            });
        </script>
    </body>
</html>