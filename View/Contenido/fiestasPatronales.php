<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      margin: auto;
      height: 300px;
  }
  .texto {
    text-align: justify;
   }
</style>
<div class="barraTitulo">
    <div class="botonAtras"></div>
    <h1>Fiestas patronales</h1>
</div>

<div class="col-sm-2 col-md-2 col-lg-2"></div>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 texto">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
          <li data-target="#myCarousel" data-slide-to="5"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto0.jpg" width="460" height="345"></img>';
            ?>
          </div>

          <div class="item">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto1.JPG" width="460" height="345"></img>';
            ?>
          </div>

          <div class="item">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto2.jpg" width="460" height="345"></img>';
            ?>
          </div>

          <div class="item">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto3.JPG" width="460" height="345"></img>';
            ?>
          </div>

          <div class="item">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto4.JPG" width="460" height="345"></img>';
            ?>
          </div>

          <div class="item">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/fiestasPatronales/suchitoto5.jpg" width="460" height="345"></img>';
            ?>
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    <p>Las fiestas patronales de Suchitoto son celebradas del 6 al 13 de diciembre en honor a Santa Lucía. En dicha fiesta se desarrollan diversas manifestaciones religiosas y populares como procesiones, presentaciones musicales, feria ganadera, quema de pólvora, elección de la reina de las fiestas, degustación de comida tradicional, bailes y ferias comerciales.</p>
    <p>Suchitoto, cuna de historia y cultura, lugar donde pareciera que el tiempo se ha dormido, donde encontramos un pueblo colonial antiguo con calles empedradas, artesanías y atuendos con diferentes diseños elaborados con añil, vistas panorámicas disfrutando de la tranquilidad, hoteles y restaurantes, costumbres y tradiciones, museo, galerías de arte y vida nocturna.</p>
</div>
<div class="col-sm-2 col-md-2 col-lg-2"></div>
<script type="text/javascript">
  urlAnterior = '';
  localStorage.setItem('urlAnterior','');
</script>