<div class="barraTitulo">
    <div class="botonAtras"></div>
    <h1>Lugares de Interes</h1>
</div>

<div class="col-sm-2 col-md-2 col-lg-2"></div>
<div class="wrapper col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <div class="nano">
        <div class="nano-content">
            <div class="list-group">
                <div id= "list-LugInteres">
                    <?php
                    include_once '../../Model/LugarDao.php';

                    $lugar = new Lugar();
                    $resultado = $lugar -> getCategoriasLugaresInteres();
                    foreach($resultado as $lugTur){
                        echo '<a href="#" class="list-group-item lugarEscogido" codigoCategoria="'.$lugTur['codigoCategoria'].'">
                            <div id="iconoPrueba" class="categoriasImagenes" style="background-image: url('.$lugTur['imgTipoLugar'].')"></div>
                            <div class="titulo_categoria">'.$lugTur['nombreCategoria'].'</div>
                            <div class="contenido_categoria">'.$lugTur['descrTipoLugar'].'</div>
                            </a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-2 col-md-2 col-lg-2"></div>