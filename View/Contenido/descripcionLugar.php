
<script type="text/javascript">
    var url = location.href;
    var index = url.indexOf('View/Contenido/descripcionLugar.php');
    if(index>=0){
        var urlActual = url.substring(index);
        localStorage.setItem('urlActual',urlActual);
        location.href=url.substring(0,index);

    }
</script>

<div class="barraTitulo">
    <div class="botonAtras"></div>
    <?php
    include_once '../../Model/LugarDao.php';
    $idLugar = $_GET['idLugar'];
    $lugar = new Lugar;
    $result = $lugar->getLugarDescById($idLugar);

    ?>
    <h1><?php echo $result[0]['nombreLugar']; ?></h1>
</div>

<div class="visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
<div class="descImg visible-xs-block visible-sm-block visible-md-inline visible-lg-inline col-xs-12 col-sm-12 col-md-8 col-lg-8">
    <div class="contenido_desc">
        <div class="imgDescripcion img-thumbnail" id="bannerImg">
            <?php
                $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                if(strlen($result[0]['imagenCabecera'])>0){
                    echo '<img src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/subidasDeUsuarios/'.$result[0]['imagenCabecera'].'"></img>';
                }

            ?>
        </div>
        <div class="decripcionImagen descImg" id="imgDesc"><?php echo $result[0]['descriptionImagenCabecera']; ?></div>
        <div class="contenido"><?php echo $result[0]['descripcionLugar']; ?></div>
        <div class="direccion"><h3>Direcion:</h3><?php echo $result[0]['direccionLugar']; ?></div>
        <div class="telefonos"><h3>Telefonos:</h3><?php echo $result[0]['telefonos']; ?></div>
    </div>
</div>
<div class="visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
<div class="visible-xs-block visible-sm-block visible-md-block visible-lg-block col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
    <div class="visible-xs-block visible-sm-block visible-md-block visible-lg-block col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div id="mapDesc" class="mapaLocalizacion"></div>
    </div>
    <div class="visible-md-inline visible-lg-inline col-md-2 col-lg-2"></div>
    <div class="contenedorQR visible-sm-block visible-md-block visible-lg-block col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="qrDesc"></div>
    </div>
</div>

<input type="text" name="latitudLugarTuristico" id="latitudLugarTuristico" value="<?php  echo $result[0]['latitudLugarTuristico']; ?>" style="display:none;"/>
<input type="text" name="longitudLugarTuristico" id="longitudLugarTuristico" value="<?php  echo $result[0]['longitudLugarTuristico']; ?>" style="display:none;"/>
<script type="text/javascript">
    urlAnterior = 'View/Contenido/lugarEscogido.php';
    localStorage.setItem('urlAnterior','View/Contenido/lugarEscogido.php');
    var urlActual = localStorage.getItem('urlActual');

    $( window ).resize(function() {
        clearTimeout(window.resizedFinished);
        window.resizedFinished = setTimeout(function(){
            redimencionarElementos();
        }, 150);
    });
    setTimeout(function(){
        getLocation($('#latitudLugarTuristico').val(),$('#longitudLugarTuristico').val());
    },500);
    var qrcode = new QRCode("qrDesc");
    var ruta = urlActual
    function makeCode () {
        qrcode.makeCode(ruta);
    }
    makeCode();
</script>