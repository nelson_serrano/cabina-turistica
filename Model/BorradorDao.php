<?php
header('Content-Type: text/html; charset=utf-8' );
class Borrador {
    function obtenerBorrador($idBorrador){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = "SELECT idBorrador, idUsuario, idMunicipio, titulo_nombre, imagenCabecera, descripcionImagenCabecera,
                    descripcion_contenido, telefonos, latitudLugar, longitudLugar, direccionLugar
                    FROM BORRADOR WHERE idBorrador= ".$idBorrador;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function obtenerUltimoBorradorSi0($idBorrador,$user){
        if($idBorrador==0){
            $servername = getenv('IP');
            $username = "theky0x";
            $password = "";
            $database = "cabinaTuristica";
            $dbport = 3306;

            // Create connection
            $db = new mysqli($servername, $username, $password, $database, $dbport);

            // Check connection
            if ($db->connect_error) {
                die("Connection failed: " . $db->connect_error);
            }
            $consulta = "SELECT MAX(idBorrador) max FROM BORRADOR WHERE idUsuario = '$user'";
            //echo $consulta;

            $db->query("SET NAMES 'utf8'");
            $db->query("SET CHARACTER SET 'utf8'");
            $result = $db->query($consulta);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    $results= $row['max'];
                }
            }
            $db->close();

            if (isset($results))
                return $results;
            else
                return 0;
        }else{
            return $idBorrador;
        }
    }

    function guardarBorrador($data,$user){
        $idBorrador = $data['idBorrador'];
        $ultimoPrev = $this->obtenerUltimoBorradorSi0($idBorrador,$user);

        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $latLon = explode(',',$data['ubicacion']);
        $latitudLugar = $latLon[0];
        $longitudLugar = $latLon[1];

        if($idBorrador=='0'){
            $update = "INSERT INTO BORRADOR(idUsuario, idMunicipio,
                        titulo_nombre, imagenCabecera, descripcionImagenCabecera,
                        descripcion_contenido, telefonos, latitudLugar, longitudLugar,
                        direccionLugar) VALUES ('".$user."',".$data['municipio']
                        .",'".$data['titulo']."','".$data['imagenCabecera']."','".$data['descripcionImagen']
                        ."','".mysql_escape_string($data['htmldata'])."','".$data['telefono']."','".$latitudLugar
                        ."','".$longitudLugar."','".$data['direccion']."')";
        }else{
            $update = "UPDATE BORRADOR SET idBorrador=".$data['idBorrador']
                    .",idUsuario='".$user."',idMunicipio=".$data['municipio'].",titulo_nombre='"
                    .$data['titulo']."',imagenCabecera='".$data['imagenCabecera']
                    ."',descripcionImagenCabecera='".$data['descripcionImagen']
                    ."',descripcion_contenido='".mysql_escape_string($data['htmldata'])."',telefonos='".$data['telefono']
                    ."',latitudLugar='".$latitudLugar."',longitudLugar='".$longitudLugar
                    ."',direccionLugar='".$data['direccion']."' WHERE idBorrador = ".$idBorrador;
        }

        //echo $update.'   ----   ';

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();

        $ultimoDesp = $this->obtenerUltimoBorradorSi0($idBorrador,$user);
        if($idBorrador=='0' && $ultimoDesp>$ultimoPrev){
            return $ultimoDesp;
        }else{
            if($idBorrador!='0'){
                return $ultimoPrev;
            }else{
                return 0;
            }
        }
    }

    function borrarBorrador($idBorrador){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $update = "DELETE FROM BORRADOR WHERE idBorrador = ".$idBorrador;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        if($db->query($update)===TRUE){
            $db->commit();
        }
        $db->close();

    }
}
?>