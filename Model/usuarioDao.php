<?php
header('Content-Type: text/html; charset=utf-8' );
class Usuario {
    function getUsers(){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = " SELECT * FROM USUARIO ORDER BY apellidoUsuario ASC, nombreUsuario ASC, idUsuario ASC,administrativo DESC";

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function getUser($user){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        $consulta = " SELECT idUsuario, nombreUsuario, apellidoUsuario, administrativo, contrasena FROM USUARIO WHERE idUsuario='".$user."'";

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function loginUsuario($usuario){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);

        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
        //echo "Connected successfully (".$db->host_info.")";

        $consulta = "SELECT u.* FROM USUARIO u WHERE u.idUsuario='".$usuario."'";
        //echo $consulta;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = $db->query($consulta);
        $results = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $results[] = $row;
            }
        }
        $db->close();

        if (isset($results))
            return $results;
    }

    function insertarUsuario($params,$user){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);
        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $update = "INSERT INTO USUARIO(idUsuario, nombreUsuario, apellidoUsuario, administrativo, contrasena)
                    VALUES ('".$params['idUsuario']."','".$params['nombreUsuario']."','".$params['apellidoUsuario']."',"
                    .$params['administrativo'].",'".$params['contra']."')";
        //echo $update;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = mysqli_query($db, $update);

        mysqli_commit($db);

        mysqli_close($db);
        return $result;
    }

    function actualizarUsusario($params,$user){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);
        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        if(strlen($params['contra'])>0){
            $update = "UPDATE USUARIO SET nombreUsuario='".$params['nombreUsuario']
                    ."',apellidoUsuario='".$params['apellidoUsuario']."',administrativo=".$params['administrativo'].",contrasena='".$params['contra']
                    ."' WHERE idUsuario='".$params['idUsuario']."'";
        }else{
            $update = "UPDATE USUARIO SET nombreUsuario='".$params['nombreUsuario']
                    ."',apellidoUsuario='".$params['apellidoUsuario']."',administrativo=".$params['administrativo']
                    ." WHERE idUsuario='".$params['idUsuario']."'";
        }
        echo $update;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = mysqli_query($db, $update);

        mysqli_commit($db);

        mysqli_close($db);
        return $result;
    }

    function eliminarUsuario($paramName, $user){
        $servername = getenv('IP');
        $username = "theky0x";
        $password = "";
        $database = "cabinaTuristica";
        $dbport = 3306;

        // Create connection
        $db = new mysqli($servername, $username, $password, $database, $dbport);
        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        $update = "DELETE FROM USUARIO WHERE idUsuario='$paramName'";
        //echo $update;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = mysqli_query($db, $update);

        mysqli_commit($db);

        mysqli_close($db);
        return $result;
    }

    function cambiarPassword($usuario,$contra){
        $array_prop = parse_ini_file('DataAcces.properties');
        $con = mysqli_connect($array_prop['servidor'], $array_prop['usuario'], $array_prop['contra'], $array_prop['base']);
        // Check connection
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $update = "UPDATE USUARIO SET passWord='".$contra."' WHERE idUsuario='".$usuario."'";
        //echo $update;

        $db->query("SET NAMES 'utf8'");
        $db->query("SET CHARACTER SET 'utf8'");
        $result = mysqli_query($con, $update);

        mysqli_commit($con);

        mysqli_close($con);
        return $result;
    }
}
?>
