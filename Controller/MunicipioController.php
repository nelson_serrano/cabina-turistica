<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    include_once '../Model/MunicipioDao.php';

    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if ($action=='obtenerMunicipio'){
            $json = $_POST['data'];

            $json = str_replace('\"','"',$json);
            $jsonDecode = json_decode($json, true);

            $codDepartamento = $jsonDecode['codDepartamento'];

            $mun = new Municipio;
            $result = $mun->obtenerMunicipio($codDepartamento);

            foreach ($result as $municipio) {
              echo '<option value="'.$municipio['idMunicipio'].'" latitud="'.$municipio['latitudMunicipio'].'" longitud="'.$municipio['longitudMunicipio'].'">'
                    .$municipio['nombreMunicipio']
                    .'</option>';
            }
        }
    }
?>