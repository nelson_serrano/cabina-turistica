<div class="barraTitulo">
    <div class="botonAtras"></div>
    <h1>Noticias</h1>
</div>

<div class="col-sm-2 col-md-2 col-lg-2"></div>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <div class="noticia_wrapper">
        <?php
            $idNoticia = $_GET['idNoticia'];

            include_once '../../Model/NoticiaDao.php';
            $not = new Noticia;
            $result = $not->obtenerNoticia($idNoticia);
        ?>
        <div class="titulo_noticia_vista"><?php echo $result['tituloNoticia']; ?></div>
        <div class="cuerpo_noticia_vista">
            <?php

                $nombreImagen = $result['imagenCabecera'];
                if(isset($nombreImagen) && strlen($nombreImagen)>0){
                    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
                    echo '<img class="imagenCabeceraNoticia" src="'.$protocol.$_SERVER['SERVER_NAME'].'/View/imagenes/subidasDeUsuarios/'.$nombreImagen.'"></img>';
                }

                $descripcionImagen = $result['descriptionImagenCabecera'];
                if(isset($descripcionImagen) && strlen($descripcionImagen)){
                    echo '<blockquote><div class="descripcionImagenNoticia">'.$descripcionImagen.'</div></blockquote>';
                }
            ?>
            <?php echo $result['contenidoNoticia']; ?>
        </div>
    </div>
</div>
<div class="col-sm-2 col-md-2 col-lg-2"></div>
<script type="text/javascript">
  urlAnterior = '';
  localStorage.setItem('urlAnterior','');
</script>