<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {

        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
    }else{
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

    if(isset($_SESSION['usuario'])){
        $user = $_SESSION['usuario'];
        $_SESSION['usuario'] = $user;
    }

    include_once '../Model/NoticiaDao.php';

    if(isset($_GET['act'])){
        $action = $_GET['act'];
        if (strcmp($action,'obtenerNoticias')==0){

            $noticia = new Noticia;
            $results = $noticia->obtenerUltimasNoticias();

            echo '<div class="noticias_banner"><h3>Ultimas noticias</h3></div>
                            <div class="nano">
                                <div class="noticias_scroller nano-content">';

            foreach($results as $res){
                echo '<div class="noticia" idNoticia="'.$res['idNoticia'].'">
                        <div class="titulo_noticia">'.$res['tituloNoticia'].'</div>
                        <div class="cuerpo_noticia">'.$res['contenidoNoticia'].'</div>
                    </div>';
            }
            echo '</div></div>';
        }
    }
?>